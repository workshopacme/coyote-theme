import React from 'react';
import {CoyoteTheme} from '../src';
import '../src/index.scss';

React.render(<CoyoteTheme />, document.getElementById('main'));
